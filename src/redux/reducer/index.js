import { combineReducers } from 'redux';
import ItemReducer from './item';

const rootReducer = combineReducers({
  Item: ItemReducer,
});

export default rootReducer;
