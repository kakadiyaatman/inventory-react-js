import {
  GET_ITEM
} from '../constants';

const INITIAL_STATE = {
  allItems: [],
  selectedItems: []
};
export default (state = JSON.parse(JSON.stringify(INITIAL_STATE)), action) => {
  switch (action.type) {
    case GET_ITEM:
      console.log('23456754321345678765432134567898765432', action)
      const allItems = action.payload.length ? action.payload.filter((data) => {
        return !data.selected;
      }): [];
      const selectedItems = action.payload.length ? action.payload.filter((data) => {
        return data.selected;
      }): [];
      return Object.assign({}, state, {allItems, selectedItems});
    default:
      return state;
  }
};
