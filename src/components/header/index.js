import React from 'react';
import {Nav, Navbar, NavLink, NavbarCollapse} from 'reactstrap';

// App.js
class Header extends React.Component {
  render() {
    return (
      <>
        <Navbar className="mt-3" expand="lg" bg="dark" variant="dark">
          <Nav className="mr-auto">
            <NavLink className="p-0">
              Login
            </NavLink>
          </Nav>
          <Nav>
            <NavLink className="p-0 mr-3">
              Home
            </NavLink>
            <NavLink className="p-0">
              About As
            </NavLink>
          </Nav>
        </Navbar>
      </>
    );
  }
}

export default Header;
