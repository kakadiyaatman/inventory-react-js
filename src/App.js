import React from 'react';
import {connect} from 'react-redux';
import {Form} from "reactstrap";
import {Button, FormControl} from "react-bootstrap";
import {InsertItem, GetItems, UpdateItem} from "./redux/action";
import objectPath from "object-path";

// App.js
export class App extends React.Component {

  state = {
    name: '',
    isDisplayErrorMessage: false,
    displayError: ''
  };

  componentDidMount() {
    const {GetItems} = this.props;
    GetItems();
  }

  setItem = () => {
    const name = document.getElementById('item');
    this.setState({name: name.value})
  };

  addItem = () => {
    const {name} = this.state;
    const {InsertItem, GetItems} = this.props;
    if (!name) {
      this.setState({isDisplayErrorMessage: true})
    } else {
      const item = {name, selected: false};
      InsertItem(item).then((data) => {
        if (!objectPath.get(data, 'response.data.status')) {
          this.setState({
            displayError: objectPath.get(data, 'response.data.message'),
            isDisplayErrorMessage: true
          })
        } else {
           this.setState({isDisplayErrorMessage: false, name: '', displayError: ''})
           GetItems();
        }
      }) 
      this.setState({isDisplayErrorMessage: false})
    }
  };

  selectedItem = (selectedItem) => {
    this.setState({selectedItem})
  };

  moveLeftToRight = () => {
    const {selectedItem} = this.state;
    const {GetItems, UpdateItem} = this.props;
    this.setState({isDisplayErrorMessage: false, displayError: ''})
    if (selectedItem) {
      const movedItem = {
        item_id: selectedItem.item_id,
        selected: 1,
        updated_at: new Date()
      };
      UpdateItem(movedItem).then(() => {
        GetItems();
      })
    }
  };

  moveRightToLeft = () => {
    const {selectedItem} = this.state;
    const {GetItems, UpdateItem} = this.props;
    this.setState({isDisplayErrorMessage: false, displayError: ''})
    if (selectedItem) {
      const movedItem = {
        item_id: selectedItem.item_id,
        selected: 0,
        updated_at: new Date()
      };
      UpdateItem(movedItem).then(() => {
        GetItems();
      })
    }
  };

  render() {
    const {name, isDisplayErrorMessage, displayError, value} = this.state;
    const {
      allItems,
      selectedItems
    } = this.props;
    return (
      <div className="container w-50 mt-5">
        <div className="d-flex">
          <FormControl
            type="item"
            id={'item'}
            value={name}
            onChange={this.setItem}
            placeholder="Enter Item"/>
          <Button className="ml-3" onClick={this.addItem} variant="primary" type="button">
            Add
          </Button>
        </div>
        {(!name && isDisplayErrorMessage) ? <label className={'error ml-0 m-2'}>{
        'Please Enter Item'}</label> : displayError ? 
        <label className={'error ml-0 m-2'}>{displayError}</label> : ''}
        <div className="d-flex justify-content-between">
          <div className="w-50 mt-4">
            <select className="form-control" size="5">
              {allItems.length ? allItems.map((item, i) => {
                return <option key={i} className="m-2" onClick={() => this.selectedItem(item)}>{item.name}</option>
              }) : []}
            </select>
          </div>
          <div className="mx-3 mt-4">
            <div className={'my-3'}><Button onClick={this.moveLeftToRight}>{">"}</Button></div>
            <div><Button onClick={this.moveRightToLeft}>{"<"}</Button></div>
          </div>

          <div className="w-50 mt-2">
            <div><b>Selected Items:</b></div>
            <select className="form-control"  size="5">
              {selectedItems.length ? selectedItems.map((item, i) => {
                return <option key={i} className="m-2" onClick={() => this.selectedItem(item)}>{item.name}</option>
              }) : []}
            </select>
          </div>
        </div>
      </div>
    );
  }
}

// AppContainer.js
const mapStateToProps = (state) => {
  const allItems = objectPath.get(state, 'Item.allItems', []);
  const selectedItems = objectPath.get(state, 'Item.selectedItems', []);
  return {
    allItems,
    selectedItems
  };
};

const AppContainer = connect(
  mapStateToProps,
  {InsertItem, GetItems, UpdateItem}
)(App);

export default AppContainer;
