import axios from 'axios';
// import baseUrl from './../config';
import {ADD_ITEM_ERROR, ADD_ITEM, GET_ITEM, GET_ITEM_ERROR} from './constants';
import objectPath from 'object-path';

export const InsertItem = (item) => {
  return axios.post('http://localhost/inventory-api/api/item-insert', item)
    .then(response => {
        response = responseAndErrorSeparator(response);
        return dispatch => {
          return returnSuccessResponse(dispatch, ADD_ITEM, ADD_ITEM_ERROR, response, objectPath.get(response, 'data'));
        };
      }
    )
    .catch(error => {
      return dispatch => {
        return returnErrorResponse(dispatch, ADD_ITEM_ERROR, error);
      };
    });
};

export const UpdateItem = (item) => {
  return axios.post('http://localhost/inventory-api/api/item-edit', item)
    .then(response => {
        response = responseAndErrorSeparator(response);
        return dispatch => {
          return returnSuccessResponse(dispatch, ADD_ITEM, ADD_ITEM_ERROR, response, objectPath.get(response, 'data'));
        };
      }
    )
    .catch(error => {
      return dispatch => {
        return returnErrorResponse(dispatch, ADD_ITEM_ERROR, error);
      };
    });
};

export const GetItems = () => {
  return axios.get('http://localhost/inventory-api/api/items')
    .then(response => {
      return dispatch => {
        if (objectPath.get(response, 'data.status')) {
          dispatch({
            type: GET_ITEM,
            payload: objectPath.get(response, 'data.data', [])
          });
          return {type: 'success', response};
        } else {
          dispatch({
            type: GET_ITEM_ERROR,
            payload: []
          });
          return {type: 'error', response};
        }
      };
    })
    .catch(error => {
      return dispatch => {
        dispatch({
          type: GET_ITEM_ERROR,
          payload: []
        });
        return {type: 'error', response: error};
      };
    });
};


function returnSuccessResponse(dispatch, successType, errorType, response, payload, previousList) {
  if (response.status === 200) {
    dispatch({
      type: successType,
      payload
    });
    return {type: 'success', response};
  } else {
    dispatch({
      type: errorType,
      payload: previousList
    });
    return {type: 'error', response};
  }
}

function responseAndErrorSeparator(response) {
  if (typeof response.data === 'string') {
    let data = response.data;
    const error = data.substr(0, data.indexOf('{'));
    if (error) {
      console.info(error);
    }
    response.data = JSON.parse(data.substr(data.indexOf('{'), response.data.length));
  }
  return response;
}

function returnErrorResponse(dispatch, errorType, error, previousList) {
  dispatch({
    type: errorType,
    payload: previousList
  });
  return {type: 'error', response: error.response};
}

